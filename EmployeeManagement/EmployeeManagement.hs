import System.IO
import System.Directory
import System.Console.ANSI
import Data.List
import Employee
import Control.Monad (unless)


-- Optiunea 1. Afisarea unui angajat dupa cnp
displayEmployee :: IO()
displayEmployee = do {
    isFile <- doesFileExist("/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt");
    if (isFile) then do {
        putStrLn "Introduceti cnp-ul angajatului cautat:";
        cnp <- getLine;
        handle <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" ReadMode;
        searchEmployeeToShow handle cnp;
        hClose handle;
    }
    else do {
        putStrLn "";
        putStrLn "Nu exista angajati inregistrati!";
        putStrLn "";
    }
}

searchEmployeeToShow :: Handle -> String -> IO()
searchEmployeeToShow handle inputCnp = do {
    eof <- hIsEOF handle;
    if (eof == True) then do {
        putStrLn "";
        putStrLn "Nu exista angajatul cu acest cnp!";
        putStrLn "";
        hClose handle;
        return ();
    }
    else do {
        empLine <- hGetLine handle;
        emp <- return (read empLine::TEmployee);
        if (inputCnp == (cnp emp)) then do {
            formatDisplayEmployee emp;
        }
        else do {
            searchEmployeeToShow handle inputCnp;
        }
    }
}

formatDisplayEmployee :: TEmployee -> IO()
formatDisplayEmployee emp = do {
    putStrLn "";
    putStrLn "Angajatul cautat are inregistrate urmatoarele date:";
    putStrLn "---------------------------------------------------";
    putStr "CNP                     ";
    putStrLn (cnp emp);
    putStr "Prenume                 ";
    putStrLn (firstName emp);
    putStr "Nume                    ";
    putStrLn (lastName emp);
    putStr "Data nasterii           ";
    putStrLn (birthDate emp);
    putStr "Adresa                  ";
    putStrLn (address emp);
    putStr "Email                   ";
    putStrLn (email emp);
    putStr "Nr de telefon           ";
    putStrLn (phone emp);
    putStr "Scoala generala         ";
    putStrLn (middleSchool emp);
    putStr "Liceu                   ";
    putStrLn (highschool emp);
    putStr "Facultate               ";
    putStrLn (college emp);
    putStr "Ultimul post            ";
    putStrLn (lastFunction emp);
    putStr "Nr ani de experienta    ";
    putStrLn (yearsExp emp);
    putStr "Aptitudini              ";
    putStrLn (skills emp);
    putStrLn "";
}


-- Optiunea 2. Afisarea tuturor angajatilor
displayAllEmployees :: IO()
displayAllEmployees = do {
    isFile <- doesFileExist("/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt");
    if (isFile) then do {
        handle <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" ReadMode;
        putStrLn "";
        putStrLn "Angajatii inregistrati sunt:";
        putStrLn "----------------------------";
        readEmployeesFromFile handle;
        putStrLn "";
        hClose handle;
    }
    else do {
        putStrLn "";
        putStrLn "Nu exista angajati inregistrati!";
        putStrLn "";
    }
}

readEmployeesFromFile :: Handle -> IO()
readEmployeesFromFile handle = do {
    eof <- hIsEOF handle;
    if (eof == True) then do {
        hClose handle;
        return ();
    }
    else do {
        empLine <- hGetLine handle;
        emp <- return (read empLine::TEmployee);
        putStrLn ((cnp emp) ++ " " ++ (lastName emp) ++ " " ++ (firstName emp));
        readEmployeesFromFile handle;
    }
}


-- Optiunea 3. Crearea/inserarea unui angajat nou
insertEmployee :: IO()
insertEmployee = do {
    putStrLn "";
    putStrLn "Creare/inserare angajat nou";
    putStrLn "----------------------------";
    putStrLn "Introduceti cnp-ul angajatului:";
    cnp <- getLine;
    putStrLn "Introduceti prenumele angajatului:";
    firstName <- getLine;
    putStrLn "Introduceti numele angajatului:";
    lastName <- getLine;
    putStrLn "Introduceti data nasterii angajatului:";
    birthDate <- getLine;
    putStrLn "Introduceti adresa angajatului:";
    address <- getLine;
    putStrLn "Introduceti email-ul angajatului:";
    email <- getLine;
    putStrLn "Introduceti nr de telefon a angajatului:";
    phone <- getLine;
    putStrLn "Introduceti scoala generala a angajatului:";
    middleSchool <- getLine;
    putStrLn "Introduceti liceul angajatului:";
    highschool <- getLine;
    putStrLn "Introduceti facultatea angajatului:";
    college <- getLine;
    putStrLn "Introduceti ultimul post a angajatului:";
    lastFunction <- getLine;
    putStrLn "Introduceti nr de ani de experienta a angajatului:";
    yearsExp <- getLine;
    putStrLn "Introduceti aptitudini a angajatului:";
    skills <- getLine;
    employee <- return (createEmployee cnp firstName lastName birthDate address email phone middleSchool highschool college lastFunction yearsExp skills);
    saveNewEmployee employee;
    putStrLn "";
    putStrLn "Angajatul a fost salvat in baza de date!";
    putStrLn "----------------------------------------";
    displayAllEmployees;
}

saveNewEmployee :: TEmployee -> IO()
saveNewEmployee emp = do {
    handle <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" AppendMode;
    hPutStrLn handle (show emp);
    hClose handle;
}


-- Optiunea 4. Stergerea unui angajat
deleteEmployee :: IO()
deleteEmployee = do {
    isFile <- doesFileExist("/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt");
    if (isFile) then do {
        putStrLn "";
        putStrLn "Introduceti cnp-ul angajatului care se doreste a fi sters:";
        cnp <- getLine;
        handle <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" ReadMode;
        searchEmployeeToDelete handle cnp [];
        hClose handle;
    }
    else do {
        putStrLn "";
        putStrLn "Nu exista angajati inregistrati!";
        putStrLn "";
    }
}

searchEmployeeToDelete :: Handle -> String -> [TEmployee] -> IO()
searchEmployeeToDelete handle inputCnp emps = do {
    eof <- hIsEOF handle;
    if (eof == True) then do {
        hClose handle;
        handleWrite <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" WriteMode;
        saveEmployees handleWrite emps;
        hClose handleWrite;
        putStrLn "";
        putStrLn "Angajatul a fost sters din baza de date!";
        putStrLn "----------------------------------------";
        displayAllEmployees;
        return ();
    }
    else do {
        empLine <- hGetLine handle;
        emp <- return (read empLine::TEmployee);
        if (inputCnp /= (cnp emp)) then do {
            emps2 <- return (addToList emp emps);
            searchEmployeeToDelete handle inputCnp emps2;
        }
        else do {
            searchEmployeeToDelete handle inputCnp emps;
        }
    }
}

addToList :: TEmployee -> [TEmployee] -> [TEmployee]
addToList emp emps = do {
    [emp] ++ emps;
}

saveEmployees :: Handle -> [TEmployee] -> IO()
saveEmployees _ [] = return ()
saveEmployees handle (emp:emps) = do {
    hPutStrLn handle (show emp);
    saveEmployees handle emps;
}


-- Optiunea 5. Modificarea unui angajat
updateEmployee :: IO()
updateEmployee = do {
    isFile <- doesFileExist("/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt");
    if (isFile) then do {
        putStrLn "";
        putStrLn "Introduceti cnp-ul angajatului pe care doriti sa il modificati:";
        cnp <- getLine;
        handle <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" ReadMode;
        employee <- return (createEmployee "" "" "" "" "" "" "" "" "" "" "" "" "");
        searchEmployeeToUpdate handle cnp [] employee;
        hClose handle;
    }
    else do {
        putStrLn "";
        putStrLn "Nu exista angajati inregistrati!";
        putStrLn "";
    }
}

searchEmployeeToUpdate :: Handle -> String -> [TEmployee] -> TEmployee -> IO()
searchEmployeeToUpdate handle inputCnp emps employee = do {
    eof <- hIsEOF handle;
    if (eof == True) then do {
        hClose handle;
        emps2 <- return (deleteBy (\x y -> x == y) employee emps);
        updateEmployeeData employee emps2;
        putStrLn "";
        putStrLn "Datele angajatului au fost modificate!";
        putStrLn "----------------------------------------";
        displayAllEmployees;
        return ();
    }
    else do {
        empLine <- hGetLine handle;
        emp <- return (read empLine::TEmployee);
        emps2 <- return (addToList emp emps);
        if ((cnp emp) == inputCnp) then do {
            searchEmployeeToUpdate handle inputCnp emps2 emp;
        }
        else do {
            searchEmployeeToUpdate handle inputCnp emps2 employee;
        }
    }
}

updateEmployeeData :: TEmployee -> [TEmployee] -> IO()
updateEmployeeData emp emps = do {
    displayUpdateMenu;
    updateOption <- getLine;
    checkUpdateOption updateOption emp emps;
}

displayUpdateMenu :: IO()
displayUpdateMenu = do {
    putStrLn "";
    putStrLn "-------------------------------";
    putStrLn "1. Adresa";
    putStrLn "2. Email";
    putStrLn "3. Nr de Telefon";
    putStrLn "4. Ultimul post ocupat";
    putStrLn "5. Nr de ani de experienta";
    putStrLn "6. Aptitudini";
    putStrLn "-------------------------------";
    putStrLn "";
    putStrLn "Alegeti ce doriti sa modificati:";
}

checkUpdateOption :: String -> TEmployee -> [TEmployee] -> IO()
checkUpdateOption option emp emps
    | option == "1" = do {
        putStrLn "";
        putStrLn "Introduceti noua adresa:";
        newAddress <- getLine;
        emp1 <- return (createEmployee (cnp emp) (firstName emp) (lastName emp) (birthDate emp) newAddress (email emp) (phone emp) (middleSchool emp) (highschool emp) (college emp) (lastFunction emp) (yearsExp emp) (skills emp));
        emps2 <- return (addToList emp1 emps);
        handleWrite <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" WriteMode;
        saveEmployees handleWrite emps2;
        hClose handleWrite;
    }
    | option == "2" = do {
        putStrLn "";
        putStrLn "Introduceti noul email:";
        newEmail <- getLine;
        emp1 <- return (createEmployee (cnp emp) (firstName emp) (lastName emp) (birthDate emp) (address emp) newEmail (phone emp) (middleSchool emp) (highschool emp) (college emp) (lastFunction emp) (yearsExp emp) (skills emp));
        emps2 <- return (addToList emp1 emps);
        handleWrite <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" WriteMode;
        saveEmployees handleWrite emps2;
        hClose handleWrite;
    }
    | option == "3" = do {
        putStrLn "";
        putStrLn "Introduceti noul nr de telefon:";
        newPhone <- getLine;
        emp1 <- return (createEmployee (cnp emp) (firstName emp) (lastName emp) (birthDate emp) (address emp) (email emp) newPhone (middleSchool emp) (highschool emp) (college emp) (lastFunction emp) (yearsExp emp) (skills emp));
        emps2 <- return (addToList emp1 emps);
        handleWrite <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" WriteMode;
        saveEmployees handleWrite emps2;
        hClose handleWrite;
    }
    | option == "4" = do {
        putStrLn "";
        putStrLn "Introduceti ultimul post:";
        newLastFunction <- getLine;
        emp1 <- return (createEmployee (cnp emp) (firstName emp) (lastName emp) (birthDate emp) (address emp) (email emp) (phone emp) (middleSchool emp) (highschool emp) (college emp) newLastFunction (yearsExp emp) (skills emp));
        emps2 <- return (addToList emp1 emps);
        handleWrite <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" WriteMode;
        saveEmployees handleWrite emps2;
        hClose handleWrite;
    }
    | option == "5" = do {
        putStrLn "";
        putStrLn "Introduceti noul nr de ani de experienta:";
        newYearsExp <- getLine;
        emp1 <- return (createEmployee (cnp emp) (firstName emp) (lastName emp) (birthDate emp) (address emp) (email emp) (phone emp) (middleSchool emp) (highschool emp) (college emp) (lastFunction emp) newYearsExp (skills emp));
        emps2 <- return (addToList emp1 emps);
        handleWrite <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" WriteMode;
        saveEmployees handleWrite emps2;
        hClose handleWrite;
    }
    | option == "6" = do {
        putStrLn "";
        putStrLn "Introduceti aptitutinile:";
        newSkills <- getLine;
        emp1 <- return (createEmployee (cnp emp) (firstName emp) (lastName emp) (birthDate emp) (address emp) (email emp) (phone emp) (middleSchool emp) (highschool emp) (college emp) (lastFunction emp) (yearsExp emp) newSkills);
        emps2 <- return (addToList emp1 emps);
        handleWrite <- openFile "/Users/robertadrianbucur/Facultate/Semestrul_VI/PLF/EmployeeManagement/employees.txt" WriteMode;
        saveEmployees handleWrite emps2;
        hClose handleWrite;
    }
    | otherwise = do {
        putStrLn "";
        putStrLn "Optiune invalida!";
        putStrLn "";
    }


-- Redirectionarea programului in functie de optiunea aleasa
checkOption :: String -> IO()
checkOption option 
    | option == "1" = do {
        clearScreen;
        setCursorPosition 5 0;
        putStrLn "Ai ales optiunea 1: Afisare angajat dupa cnp";
        putStrLn "";
        displayEmployee;
    }
    | option == "2" = do {
        clearScreen;
        setCursorPosition 5 0;
        putStrLn "Ai ales optiunea 2: Afisare toti angajati";
        putStrLn "";
        displayAllEmployees;
    }
    | option == "3" = do {
        clearScreen;
        setCursorPosition 5 0;
        putStrLn "Ai ales optiunea 3: Creare angajat";
        putStrLn "";
        insertEmployee;
    }
    | option == "4" = do {
        clearScreen;
        setCursorPosition 5 0;
        putStrLn "Ai ales optiunea 4: Stergere angajat";
        putStrLn "";
        deleteEmployee;
    }
    | option == "5" = do {
        clearScreen;
        setCursorPosition 5 0;
        putStrLn "Ai ales optiunea 5: Modificare date angajat";
        putStrLn "";
        updateEmployee;
    }
    | option == "9" = do {
        putStrLn "";
        putStrLn "La revedere!";
        putStrLn "";
    }
    | otherwise = do {
        clearScreen;
        setCursorPosition 5 0;
        putStrLn "";
        putStrLn "Optiune invalida!";
        putStrLn "";
    }


-- Afisarea meniului principal
displayMenu :: IO()
displayMenu = do {
    clearScreen;
    setCursorPosition 5 20;
    putStrLn "Meniu aplicatie Evidenta Angajati";
    setCursorPosition 6 20;
    putStrLn "-------------------------------";
    setCursorPosition 7 20;
    putStrLn "| 1. Afisare angajat dupa cnp |";
    setCursorPosition 8 20;
    putStrLn "| 2. Afisare toti angajati    |";
    setCursorPosition 9 20;
    putStrLn "| 3. Creare angajat           |";
    setCursorPosition 10 20;
    putStrLn "| 4. Stergere angajat         |";
    setCursorPosition 11 20;
    putStrLn "| 5. Modificare date angajat  |";
    setCursorPosition 12 20;
    putStrLn "| 9. Terminare                |";
    setCursorPosition 13 20;
    putStrLn "-------------------------------";
    setCursorPosition 14 20;
    putStrLn "";
    setCursorPosition 15 20;
    putStr "Introduceti o optiune:";
    hFlush stdout;
}

prompt :: IO()
prompt = do {
    displayMenu;
    option <- getLine;
    checkOption option;
    unless (option == "9") $ do {
        putStrLn "Apasa enter pentru a vedea meniul";
        wait <- getLine;
        prompt;
    }
}

main :: IO()
main = do
    prompt