module Employee (
    TEmployee,
    cnp,
    firstName,
    lastName,
    birthDate,
    address,
    email,
    phone,
    middleSchool,
    highschool,
    college,
    lastFunction,
    yearsExp,
    skills,
    createEmployee,
) where

import System.IO

data TEmployee = TEmployee {
    cnp :: String,
    firstName :: String,
    lastName :: String,
    birthDate :: String,
    address :: String,
    email :: String,
    phone :: String,
    middleSchool :: String,
    highschool :: String,
    college :: String,
    lastFunction :: String,
    yearsExp :: String,
    skills :: String
}   deriving (Read,Show, Eq)

createEmployee :: String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> TEmployee
createEmployee cnp firstName lastName birthDate address email phone middleSchool highschool college lastFunction yearsExp skills = TEmployee { cnp = cnp, firstName = firstName, lastName = lastName, birthDate = birthDate, address = address, email = email, phone = phone, middleSchool = middleSchool, highschool = highschool, college = college, lastFunction = lastFunction, yearsExp = yearsExp, skills = skills }