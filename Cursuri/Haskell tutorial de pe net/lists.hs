-- Lists
primeNumbers = [3, 5, 7, 11]

morePrimes = primeNumbers ++ [13, 17, 19, 23, 29]    -- it does the union of the two lists

favNums = 2 : 7 : 21 : 66 : []      -- create a list; `:` it's like an append operator

multList = [[3, 5, 7], [11, 13, 17]]        -- list of lists

morePrimes2 = 2 : morePrimes        -- append 2 to the list

lenPrime = length morePrimes2       -- gets the length

revPrime = reverse morePrimes2      -- reverse the list

isListEmpty = null morePrimes2      -- verifies if the list is empty

secondPrime = morePrimes2 !! 1      -- indexing: it takes the second value from the list; 0 indexing => 1 is the second item

firstPrime = head morePrimes2       -- gets the first element (the head)

lastPrime = last morePrimes2        -- gets the last elemenet

primeInit = init morePrimes2        -- everything but the last value

first3Primes = take 3 morePrimes2       -- takes the first 3 elements

removedPrimes = drop 3 morePrimes   -- remove the first 3 items

is7InList = 7 `elem` morePrimes2    -- verify if 7 is in the list

maxPrime = maximum morePrimes2      -- gets the max value

minPrime = minimum morePrimes2      -- gets the min value

newList = [2, 3, 5]

productPrimes = product newList     -- it does the product of all list

zeroToTen = [0..10]         -- a list with the numbers from 0 to ten

evenList = [2, 4..20]       -- a list with even numbers from 2 to 20

letterList = ['A', 'C'..'Z']        -- a list of letters from 2 to 2

infinPow10 = [10, 20..]         -- an infinit a list (is created only when you need it, like you call it)

many2s = take 10 (repeat 2)     -- `(repeat 2)` makes an infinit list with 2; `take 10` takes the first 10 elements

many3s = replicate 10 3         -- genereates a list of 10 3s

cycleList = take 10 (cycle [1, 2, 3, 4, 5])     -- `cycle` generates the same list over and over; `take` take a certain number of elements

lsitTimes2 = [x * 2 | x <- [1..10]]     -- mapping a list; in this case every element from the list [1..10] is multiplied by 2