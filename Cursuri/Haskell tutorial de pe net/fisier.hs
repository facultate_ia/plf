-- Ilustrare protocol de lucru cu fisiere in Haskell
-- Import capabilitati de lucru cu fisiere
import System.Directory
import System.IO

-- Import capabilitati de lucru cu ansi-terminal
import System.Console.ANSI

--Declarare tip inregistrare fisier
data TStud=Stud {matricol::Int,nume::String,prenume::String}
  deriving (Read,Show)
  
-- Declarare tip inregistrare pentru evidenta fisier de lucru curent
data TFLC=FLC String
  deriving (Read,Show)


-- Creare fisier Haskell vid
-- Se inregistreaza si ca fisier de lucru curent  
creEHF::String->IO Int
creEHF nF=do{
  efnF<-doesFileExist(nF);
  if(efnF) then return 1
           else do{
                    --Deschidere fisier in scriere 
                    fh<-openFile nF WriteMode;
                    
                    -- Inchidere fisier+scriere <EOF>+eliberare handle
                    hClose fh;

                    -- Inca un exemplu de creare a unui fisier 
                    flh<-openFile "EFLC.dat"  WriteMode;
                    
                    -- In care se scrie o inregistrare ca sir de caractere
                    -- se foloseste instanta clasei Show generata de compilator
                    hPutStrLn flh (show (FLC nF));
                    hClose flh;                     
                    return 0
                  }
}


-- Adaugare date in fisier de handle exterior specificat
adRecFApp::Handle->IO()
adRecFApp hf=do
{
   clearScreen;
   setCursorPosition 10 30;
   putStr "Adaugati(D,N):";
   hFlush stdout;
   ras<-getLine;
   if(ras=="D") then do{
                          setCursorPosition 11 30;
                          putStr "Matricol:">>hFlush stdout;
                          mS<-getLine;
                          mInt<-return(read mS::Int);
                          
                          setCursorPosition 12 30;
                          putStr "Nume:">>hFlush stdout;
                          numeS<-getLine;

                          setCursorPosition 13 30;
                          putStr "Prenume:">>hFlush stdout;
                          prenumeS<-getLine;
                          -- Inca un exemplu de scriere in fisier                        
                          hPutStrLn hf (show (Stud {matricol=mInt,nume=numeS,prenume=prenumeS}));
                          adRecFApp hf
                       }
                else return ()
                       
}    
            
-- Afisare neglijenta date fisier
-- pe ecran
stRecF::Handle->IO()
stRecF hf=do{
              eof<-hIsEOF hf;
              if(eof==True) then do {
                                     hClose hf;
                                     return ()
                                    }
                      else do{
                               -- Citire inregistrare ca sir de caractere
                               recS<-hGetLine hf;
                               -- Parsare linie citita+casting la TStud
                               recFI<-return (read recS::TStud);
                               str recFI;
                               stRecF hf
                              };
}

-- Afisare inregistrare citita din fisier
-- A se vedea utilitaea numelor campurilor inregistrarii fisierului
str::TStud->IO()
str inr=do{
            Just(lc,_)<-getCursorPosition;
            setCursorPosition lc 30;(putStr $ "Nume student:"++(nume inr))>>hFlush stdout;
            setCursorPosition (lc+1) 30;(putStr $ "Prenume student:"++(prenume inr))>>hFlush stdout;
            putStrLn "";
            putStrLn ""             
          }

-- Testare          
main::IO()
main=do{
  -- Creare fisier cu nume hardcodat
  rezC<-creEHF "Stud.dat";
  if (rezC==0) then putStrLn "Fisier vid creat...">>getLine>>=(\temp->putStrLn temp)
               else putStrLn "Probleme la crearea fisierului vid">>getLine>>=(\temp->putStrLn temp);
    
   
  -- Adaugare date in coada fisierului de nume hardcodat
  hfa<-openFile "Stud.dat" AppendMode;
  adRecFApp hfa;
  hClose hfa;
    
  -- Consultare fisier de lucru curent
  clearScreen;
  setCursorPosition 10 30;
  putStrLn "Testare <stRecF>....";
    
  hfi<-openFile "Stud.dat" ReadMode;
  stRecF hfi;    
  tt<-getLine;
  putStrLn tt;

};

                