-- One line comment

{-
Multiline
comment
-}

import Data.List    --import module/package
import System.IO

-- Int -> min: -2^63 , max: 2^63
maxInt = maxBound :: Int
minInt = minBound :: Int

-- Integer: unbounded whole number (how much your memory can hold)

-- Float: single precision floating point
-- Double: double precision floating point
bigFloat1 = 3.99999999999 + 0.00000000005        -- 4.00000000004
bigFloat2 = 3.999999999999 + 0.000000000005     -- 4.0000000000039995

-- Bool: True or False
-- Char: single quotes
-- Tuples

always5 :: Int
always5 = 5

sumOfNums = sum [1..1000]       -- 500500; sum -> function

addEx = 5 + 5
subEx = 5 - 5
multEx = 5 * 5
divEx = 5 / 5

modEx = mod 5 4     -- prefix operator (function)
modEx2 = 5 `mod` 4  -- infix operator

negNumEx = 5 + (-4)

-- in terminal type `:t`
-- example `:t sqrt`
-- output `sqrt :: Floating a => a -> a`
-- details about the function

num9 = 9 :: Int     -- constant
sqrtOf9 = sqrt (fromIntegral num9)  -- fromIntegral: function which converts from Int to float
--  round: converts float to Int

-- Built in math functions
piVal = pi
ePow9 = exp 9
logOf9 = log 9
squared9 = 9 ** 2
truncateVal = truncate 9.999
roundVal = round 9.999
ceilingVal = ceiling 9.999
floorVal = floor 9.999

-- Also sin, cos, tan, asin, acos, atan, sinh, cosh, tanh, asinh, acosh, atanh

trueAndFalse = True && False
trueOrFale = True || False
notTrue = not(True)