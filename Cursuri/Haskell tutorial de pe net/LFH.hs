import System.Directory
import System.IO
import System.Console.ANSI

data TStud=Stud {matricol::Int, nume::String, prenume::String}
    deriving (Read, Show)

data TFLC=FLC String
    deriving (Read, Show)

creEHF::String->IO Int
creEHF nF=do{
    efnF<-doesFileExist(nF);
    if(efnF) then return 1
    else do{
        fh<-openFile nF WriteMode;
        hClose fh;
        flgh<-openFile "EFLC.dat" WriteMode;
        hPutStrLn flgh (show (FLC nF));
        hClose flgh;
        return 0
    }
}

adRecFApp::Handle->IO()
adRecFApp hf=do{
    clearScreen;
    setCursorPosition 10 30;
    putStr "Adaugati(D,N):";
    hFlush stdout;
    ras<-getLine;
    if(ras=="D") then do{
        setCursorPosition 11 30;
        putStr "Matricol:">>hFlush stdout;
        mS<-getLine;
        mInt<-return(read mS::Int);

        setCursorPosition 12 30;
        putStr "Nume:">>hFlush stdout;
        numeS<-getLine;

        setCursorPosition 13 30;
        putStr "Prenume:">>hFlush stdout;
        prenumeS<-getLine;

        hPutStrLn hf (show (Stud {matricol=mInt, nume=numeS, prenume=prenumeS}))
        adRecFApp 
    }
    else do{
        hClose hf;
        return ()
    }
}

main::IO()
main= do
 {
       clearScreen;
       mesaj <- creEHF "Stud.dat";
       if (mesaj == 0) then putStrLn "Fisier vid creat..." >>getLine>>=(\temp -> putStrLn temp)
                             else putStrLn "Fisierul exista." >>getLine>>=(\temp -> putStrLn temp)
 }

stRecF::Handle ->IO()
stRecF f = do {
    eof <- hIsEOF f;
    if (eof) then return()
    else do {
        recS <- hGetLine;
        recFI <- return (read recS::TStud);
        recFI
    };
        stRecF;
  }

adRecApp:: Handle -> IO()
adRecApp handle = do {
  setCursorPosition 10 30;
  putStr "Adaugati (D, N)?";
  hFlush stdout;
  ras <-getLine;
  if (ras == "DA") then do {
    setCursorPosition 11 30;
    putStr "Matricol:">>hFlush stdout;
    ms <-getLine;
    mInt <-return (read ms::Int);

    setCursorPosition 12 30;
    putStr "Nume:">>hFlush stdout;
    numeS <-getLine;

    setCursorPosition 13 30;
    putStr "Prenume:">>hFlush stdout;
    prenumeS <-getLine;

    hPutStrLn (show (Stud {matricol = mInt, nume= numeS, prenume= prenumeS}));
    adRecFApp handle;
    }
    else do {
      hClose handle;
      return ()
      }
  }