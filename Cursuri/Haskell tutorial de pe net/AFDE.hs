-- Exemplu de modul Haskell
-- Modulul contine cpabilitati utile in afisarea formatata a datelor
-- pe ecran in aplicatiile Haskell
-- Modulul mai poate fi optimizat!!!!

module AFDE(
  TInr,
  TDF,
  extract_DA,
  extract_DF,
  
  genSRap,
  stLInr,
  stInr,
  stAntet,
  mType,
  subl,
  repcar,
  luns,
  ltza
) where

import System.Console.ANSI

import System.IO

type TInr=[String]
type TDF=[(Int,Int)]

-- Cod Haskell pentru afisarea formatata a datelor pe ecran
-- Afisarea formatata are urmatoarele premize:
--   Datele de afista vor veni sub forma de string-uri intr-o lista
--   Informatiile de formatare vin de asemenea intr-o lista

-- ******************************************************************
-- Subliniere pe linie specificata de la coloana de start specificata
subl::Int->Int->Int->Char->IO()
subl l c lun car=do
{
  setCursorPosition l c;
  putStr (repcar car lun);hFlush stdout;
}

-- Replicare caracter de un numar de ori specificat
repcar::Char->Int->String
repcar _ 0=[]
repcar car nr=car:(repcar car (nr-1))

-- Determinare lungime subliniere aferenta afisare pe ecran
-- inregistrare utilizator
-- Se iau in calcul inclusiv barele de separare a coloanelor
-- Lungimea se intoarce incapsulata IO
luns::[(Int,Int)]->IO Int
luns l=do
{
  nb<-return (length l);
  ltzaf<-return (ltza l);
  return (nb+ltzaf);
}

-- Determinare lungime exclusiv zone de afisare coloane inregistrae
ltza::[(Int,Int)]->Int
ltza []=0
ltza ((lza,_):rl)=lza+(ltza rl)
  
-- ******************************************************************

extract_DA::[(String,(Int,Int))]->[String]
extract_DA []=[]
extract_DA ((ca,_):rl)=(ca:(extract_DA rl))


extract_DF::[(String,(Int,Int))]->[(Int,Int)]
extract_DF []=[]
extract_DF ((_,df):rl)=(df:(extract_DF rl))

-- Generare raport cu date furnizate sub forma de lista
-- de inregistrari
genSRap::Int->TInr->[TInr]->TDF->IO()
genSRap cs dantet lInr ldf=do
{
  lza<-(luns ldf);
  ltza<-return (lza+1);
  
  stAntet cs dantet ldf;
  stLInr cs lInr ldf;

-- Subliniere finala
  Just(lc,_)<-getCursorPosition;
  setCursorPosition lc cs;
  subl lc cs ltza '-';    
}
 
                                             
-- Afisare lista de inregistrari
stLInr::Int->[[String]]->[(Int,Int)]->IO()
stLInr _ [] _=return ();
stLInr cs (inr:linr) ldf=do
{
  stInr cs inr ldf;
  stLInr cs linr ldf
}  

-- Afisare titlu si antet raport
stAntet::Int->[String]->[(Int,Int)]->IO()
stAntet cs ldca ldf=do
{
--  clearScreen;
  lza<-(luns ldf);
  ltza<-return (lza+1);
  putStrLn "";  
  mType 15 "LISTA DATE PERSONALE ANGAJATI" ltza 1;  
  putStrLn "";
  subl 3 15 ltza '-';  
  putStrLn "";

  -- Afisare antet
  stInr 15 ldca ldf;

  -- Subliniere pozitionata+<retur de car/salt la linie noua>
  subl 5 15 ltza '-';  

  putStrLn "";  
}

-- Afisare inregistrare
stInr::Int->[String]->[(Int,Int)]->IO()

stInr cs [col] [(lza,cal)]=do
{
  stcol cs col lza cal;
  Just(lc,_)<-getCursorPosition;
  cbv<-return (cs+lza+1);
  setCursorPosition lc cbv;
  putStr "|";hFlush stdout;
  putStrLn "";
}
  
stInr cs (col:rlc) ((lza,cal):rldf)=do
{
  stcol cs col lza cal;
  stInr (cs+lza+1) rlc rldf
}

-- Afisare coloana
stcol::Int->String->Int->Int->IO()

-- Afisare cu aliniere la stanga
stcol cs ic lza 0=do
{
  Just(lc,_)<-getCursorPosition;
  setCursorPosition lc cs;
  putStr "|";hFlush stdout;
  putStr ic;hFlush stdout;
  putStr (repcar ' ' (lza-(length ic)));hFlush stdout;
}

-- Afisare centrata
stcol cs ic lza 1=do
{
  Just(lc,_)<-getCursorPosition;
  putStr "|";hFlush stdout;
  depl<-return ((lza-(length ic)) `div` 2);
  css<-return (cs+depl+1);
  setCursorPosition lc css;
  putStr ic;hFlush stdout;
  putStr (repcar ' ' (lza-(length ic))); hFlush stdout;
}

-- Afisare cu aliniere la dreapta
stcol cs ic lza 2=do
{
  Just(lc,_)<-getCursorPosition;
  putStr "|";hFlush stdout;
  depl<-return (lza-(length ic));
  putStr (repcar ' ' depl);hFlush stdout;
  setCursorPosition lc (cs+depl+1);
  putStr ic;hFlush stdout;
}

-- Afisare centrata mesaj
-- pozitionata si cu lungime zona de afisare specificata
mType::Int->String->Int->Int->IO()
mType cs mes lza ca=do
{  
  Just(lc,_)<-getCursorPosition;
  depl<-return ((lza-(length mes)) `div` 2);
  css<-return (cs+depl+1);
  setCursorPosition lc css;
  putStr mes;hFlush stdout;
  putStr (repcar ' ' (lza-(length mes))); hFlush stdout;
}



  
