import Data.List
import System.IO

fib = 1 : 1 : [a + b | (a, b) <- zip fib (tail fib)]

-- 1st : fib = 1 and (tail fib) = 1
-- [1,1,2] : a:1 + b:1 = 2

-- 2nd : fib = 1 and (tail fib) = 2
-- [1,1,2,3] : a:1 + b:2 = 3

fib300 = fib !! 300