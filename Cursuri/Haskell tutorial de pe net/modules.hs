import Data.List
import System.IO

-- Define a module
-- import SampFunctions where you want to import it
module SampFunctions (getClass, doubleEvenNumber) where

doubleEvenNumber y =
if (y `mod` 2 /= 0)
    then y
    else y * 2


getClass :: Int -> String
getClass n = case n of
    5 -> "Go to Kindergarten"
    6 -> "Go to elementary school"
    _ -> "Go away"