import Data.List
import System.IO

-- enum
data BaseballPlayer = Pitcher
                    | Catcher
                    | Infielder
                    | Outfield
                deriving Show   -- to be able to print it like a string

barryBonds :: BaseballPlayer -> Bool
barryBonds Outfield = True

barryInOF = print(barryBonds Outfield)


-- custom type
data Customer = Customer String String Double
    deriving Show

tomSmith :: Customer
tomSmith = Customer "Tom Smith" "123 Main" 20.50

getBalance :: Customer -> Double
getBalance (Customer _ _ b) = b


data RPS = Rock | Paper | Scissors

shoot :: RPS -> RPS -> String
shoot Paper Rock = "Paper beates Rock"
shoot Rock Scissors = "Rock beates Scissors"
shoot Scissors Paper = "Scissors beat Paper"
shoot Scissors Rock = "Scissors loses to Rock"
shoot Paper Scissors = "Paper loses to Scissors"
shoot Rock Paper = "Rock loses to Paper"
shoot _ _ = "Error"


data Shape = Circle Float Float Float | Rectangle Float Float Float Float
    deriving Show

-- kind of like inheritance
area :: Shape -> Float
area (Circle _ _ r) = pi * r ^ 2
area (Rectangle x y x2 y2) = (abs (x2 - x)) * (abs $ y2 - y)
-- dollar sign gets rid of paranthesis


sumVal = putStrLn (show (1 + 2))
-- dot operator chain the functions
sumVal2 = putStrLn . show $ 1 + 2

areaOfCircle = area (Circle 50 60 20)
areaOfRect = area $ Rectangle 10 10 100 100


data Employee = Employee {  name :: String,
                            position :: String,
                            idNum :: Int
                            } deriving (Eq, Show)

samSmith = Employee {name = "Sam Smith", position = "Manager", idNum = 1000}
pamMarx = Employee {name = "Pam Marx", position = "Sales", idNum = 1001}

isSamPam = samSmith == pamMarx
samSmithData = show samSmith


data ShirtSize = S | M | L

-- overloading the equality
instance Eq ShirtSize where
    S == S = True
    M == M = True
    L == L = True
    _ == _ = False

-- overloading the show (which is like toString)
instance Show ShirtSize where
    show S = "Small"
    show M = "Medium"
    show L = "Large"

smallAvailable = S `elem` [S, M, L] -- verifies if S is in the list
theSzie = show S                    -- shows (or writes to String) the S


class MyEq a where
    areEqual :: a -> a -> Bool

instance MyEq ShirtSize where
    areEqual S S = True
    areEqual M M = True
    areEqual L L = True
    areEqual _ _ = False

newSize = areEqual M M