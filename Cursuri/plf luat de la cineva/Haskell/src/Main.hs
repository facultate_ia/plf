import System.IO

import System.Directory

import System.Console.ANSI

import Data.Char

import Text.Read

data TColumnValue=I Int|F Float|D Double|S String
  deriving (Read,Show)
  
data TColumnInfo=ColumnInfo {name::String, dataType::String, maxLength::Int}
  deriving (Read,Show)
  
data TColumnValueResult=ColumnValueResult {result::TColumnValue, attempts::Int}
  deriving (Read,Show)
  
data TRecord=Record {columnValues::[TColumnValue]}|Header {columnInfos::[TColumnInfo]}
  deriving (Read,Show)

data TFile=File {records::[TRecord]}
  deriving (Read,Show)

noDatabaseLength::Int
noDatabaseLength=length "| no database exists"

noSelectedDatabaseLength::Int
noSelectedDatabaseLength=length"| DB: no database selected"

options::[String]
options=["1","2","3","4","5","6","7","8"]

currentDatabaseInfo::String
currentDatabaseInfo = "currentDatabase.info"

existingDatabases::String
existingDatabases = "existingDatabases.info"
  
replicateCharacter::Char->Int->[Char]
replicateCharacter _ 0=[]
replicateCharacter character nr=[character]++(replicateCharacter character (nr-1))


getZoneLength::[TColumnInfo]->Int
getZoneLength []=0
getZoneLength (colInfo:rl)=(maxLength colInfo)+(getZoneLength rl)

validateOption::String->Bool
validateOption opt |(verapop opt options)=True
             |otherwise=False
  
verapop::String->[String]->Bool
verapop _ []=False
verapop opv (op:rl) |(opv==op)=True
                     |otherwise=verapop opv rl

appendTo::[TColumnValue]->TColumnValue->[TColumnValue]
appendTo columnValues columnValue = columnValues ++ [columnValue];

appendToRecords::[TRecord]->TRecord->[TRecord]
appendToRecords records record = records ++ [record];

appendRecordListToRecords::[TRecord]->[TRecord]->[TRecord]
appendRecordListToRecords records record = records ++ record;

writeTFile::TFile->String->IO()
writeTFile tfile fileName=do {
    hfl<-openFile (fileName ++ ".table") WriteMode;
    hPutStrLn hfl (show (tfile));
    hClose hfl;
}

readTFile::String->IO TFile
readTFile fileName=do {
    handle<-openFile (fileName ++ ".table") ReadMode;
    line<-hGetLine handle;
    tfile<-return (read line::TFile);
    hClose handle;
    return tfile;
}

appendStringToList::[String]->String->[String]
appendStringToList list string = (list ++ [string]);

drawUI::IO()
drawUI=do
{
  clearScreen;
  
  dbsExists<-verifyDatabases;
  case dbsExists of
  {
    1->do
       {
        setCursorPosition 1 10;
        putStrLn $ "no database exists";
       };      
    2->do
    {
        setCursorPosition 1 10;
        handle<-openFile currentDatabaseInfo ReadMode;
        eofbd<-hIsEOF handle;
        printUsedDb eofbd handle;
        hClose handle;
    };
  };
  
}

verifyDatabases::IO Int
verifyDatabases=do
{        
 databasesExists<-doesFileExist existingDatabases;
 if(databasesExists) 
 then  do
 {
   hDatabaase<-openFile existingDatabases ReadMode;
   eofbd<-hIsEOF hDatabaase;
   if(eofbd) then (hClose hDatabaase)>>(return 1)
             else (hClose hDatabaase)>>(return 2);          
 }
 else do {
    hDatabaase<-openFile existingDatabases WriteMode;
    return 1;
 };
}  

printUsedDb::Bool->Handle->IO()
printUsedDb isEof handle= do {
    if(isEof) then do {
         putStrLn $ "DB: no database selected" ;
       } else do {
         databaseInUseName<-hGetLine handle;
         putStrLn $ "DB:  " ++ databaseInUseName;
    }
}

drawMenu::IO String
drawMenu=do
{
  clearScreen;
  setCursorPosition 6 10;
  clearFromCursorToScreenEnd;
  setCursorPosition 8 10;
  putStrLn "1 - Show existing databases";  
  setCursorPosition 9 10;
  putStrLn "2 - Select database";  
  setCursorPosition 10 10;
  putStrLn "3 - Create new database";  
  setCursorPosition 11 10;
  putStrLn "4 - Show existing tables";
  setCursorPosition 12 10;
  putStrLn "5 - Create new table";  
  setCursorPosition 13 10;
  putStrLn "6 - Insert into table";  
  setCursorPosition 14 10;
  putStrLn "7 - View table";
  setCursorPosition 15 10;
  putStrLn "8 - Exit";
  setCursorPosition 17 10;
  putStr " Choose an option (1, 2, 3, 4, 5, 6, 7, 8): ";
  hFlush stdout;
  opt<-getLine;
  case (validateOption opt) of
    True->return opt
    False->drawMenu
}

executeOption::String->IO()
executeOption("1")=do{
       clearScreen;
       setCursorPosition 3 10;
       putStr "Press any key to return to the main menu.";
       hndl<-openFile existingDatabases ReadMode;
       databases<-(getExistingDatabases hndl);
       hClose hndl;
       printExistingDatabases databases 5 53 ; 
       getLine;
       main;
}

executeOption("2")=do{
       clearScreen;
       hndl<-openFile existingDatabases ReadMode;
       databases<-(getExistingDatabases hndl);
       hClose hndl;
       printExistingDatabases databases 2 53 ;
       selectDatabase;
       getLine;
       main;
}

executeOption("3")=do{
     clearScreen;
     createDatabase;
     getLine;
      main;
}     

executeOption("4")=do{
      clearScreen;
      printExistingTables;
      getLine;
      main;
}      

executeOption("5")=do{
     clearScreen;
     createNewTable;
     main;
}

executeOption("6")=do{
       clearScreen;
       setCursorPosition 0 0;
       putStrLn (replicateCharacter '-' 120);
       insertIntoTable;
       main;             
}

executeOption("7")=do{
  clearScreen;
  setCursorPosition 0 0;
  putStrLn (replicateCharacter '-' 120);
  printExistingTables;
  showTableToUI;
  getLine;
  clearFromCursorToScreenEnd;
  main                 
}

executeOption("8")=do{
    setCursorPosition 20 10;
    putStr "The application is going to close";
    hFlush stdout;
    tasta<-getLine;
    putStrLn tasta; 
}      


--1. SHOW EXISTING DATABASES


getExistingDatabases::Handle->IO [String]
getExistingDatabases hndl = do {
   eof<-hIsEOF hndl;
   if(eof == False) then do {
          line<-hGetLine hndl;
          rest<-getExistingDatabases hndl;
        return (line:rest);
     } else do {
         return [];
        }
}


printExistingDatabases::[String]->Int->Int->IO()
printExistingDatabases databases yCoord xCoord = do {
    if((length databases) == 0)
    then do {
         return();
    } else do {
        setCursorPosition yCoord (xCoord - 45);
        putStrLn (replicateCharacter '-' 100);
        setCursorPosition (yCoord + 1) xCoord;
        putStr (head databases);
        setCursorPosition (yCoord + 2) (xCoord - 45);
        putStrLn (replicateCharacter '-' 100);
        printExistingDatabases (tail databases) (yCoord + 4) xCoord;
    }
}

-- 2. SELECT DATABASE

selectDatabase::IO()
selectDatabase = do 
{
       setCursorPosition 20 10;
       putStr "Name of database: ";
       databaseName<-getLine;
       registeredDbs<-getLinesFromFileAsList existingDatabases;
      if(elem (databaseName ++ ".mydb") registeredDbs) then do {
          setCurrentDatabaseName (databaseName ++ ".mydb");
          setCursorPosition 23 10;
          putStr ("Database " ++ databaseName ++ " has been succesfully selected.");
        } else do {
          setCursorPosition 23 15;
          putStr ("Database  " ++ databaseName ++ " is not registered.");
        }
}

  
getLinesFromFileAsList :: FilePath -> IO [String]
getLinesFromFileAsList path = do { 
    contents <- readFile path;
    return (lines contents);
}


setCurrentDatabaseName::String->IO()
setCurrentDatabaseName databaseName = do {
     handle<-openFile currentDatabaseInfo WriteMode;
     hPutStrLn handle databaseName;
     hClose handle;
}

-- 3. Create new Database

createDatabase::IO()
createDatabase = do 
{
    setCursorPosition 5 10;
    putStr "Enter a name for the new database: ";
    newDatabaseName<-getLine;
    handleDatabase<-openFile (newDatabaseName ++ ".mydb") WriteMode;
    hFlush stdout;
    hClose handleDatabase;
    addNewDatabaseToExistings (newDatabaseName ++ ".mydb");
    setCurrentDatabaseName (newDatabaseName ++ ".mydb");
    setCursorPosition 10 10;
    putStr ("Database " ++ newDatabaseName ++ " has been succesfully created");
}

addNewDatabaseToExistings::String->IO()
addNewDatabaseToExistings newDatabaseName = do {
    handle<-openFile existingDatabases AppendMode;
    hPutStrLn handle newDatabaseName;
    hClose handle;
}


-- 4. SHOW EXISTING TABLES

printExistingTables::IO()
printExistingTables = do{
    currentDbHandler<-openFile currentDatabaseInfo ReadMode;
    eof<-hIsEOF currentDbHandler;
    hClose currentDbHandler;
    if(eof == True) then do {
      setCursorPosition 6 10;
      putStr "The database is not selected.";
    } else do {
      setCursorPosition 3 10;
      putStr "Press any key to return to the main menu.";
      handle<-openFile currentDatabaseInfo ReadMode;
      currentDb<-hGetLine handle;
      hndl<-openFile currentDb ReadMode;
      tables<-(getExistingTablesFromDb hndl);
      hClose hndl;
      hClose handle;
      printTables tables 5 53;
    }
}


getExistingTablesFromDb::Handle->IO [String]
getExistingTablesFromDb hndl = do {
   eof<-hIsEOF hndl;
   if(eof == False) then do {
        line<-hGetLine hndl;
        rest<-getExistingTablesFromDb hndl;
        return (line:rest);
   } else do {
          return [];
       }
}


printTables::[String]->Int->Int->IO()
printTables tables yCoord xCoord = do {
    if((length tables) == 0)
   then do {
      return();
    } else do {
       setCursorPosition yCoord (xCoord - 45);
       putStrLn (replicateCharacter '-' 100);
       setCursorPosition (yCoord + 1) xCoord;
       putStr (head tables);
       setCursorPosition (yCoord + 2) (xCoord - 45);
       putStrLn (replicateCharacter '-' 100);
       printTables (tail tables) (yCoord + 4) xCoord;
    }
}


-- 5. Add new table

createNewTable::IO()
createNewTable = do {
        currentDbHandler<-openFile currentDatabaseInfo ReadMode;
        eof<-hIsEOF currentDbHandler;
         hClose currentDbHandler;
        if(eof == True) then do {
        setCursorPosition 15 10;
        putStr "The database is not selected.";
        a<-getLine;
        putStr a;
        } else do {
          createTable 1 10;
        }
}

createTable::Int->Int->IO()
createTable yCoord xCoord=do{
    setCursorPosition yCoord xCoord;
    putStr "Name of table: ";
    hFlush stdout;
    tableName<-getLine;
    addTableToDatabase tableName;
    columnInfos<-getColumnInfosFromUser (yCoord + 3) xCoord;
    header<-return (Header columnInfos);
    newTFIle<-return (File {records=[header]});
    writeTFile newTFIle (tableName);
    readed<-readTFile (tableName);
    clearScreen;
    showTable readed tableName 1 1;
    setCursorPosition (yCoord + 40) xCoord;
    putStr $ "The table has been succesfully created."++tableName;
    hFlush stdout;
    tasta<-getLine; 
    putStrLn tasta;
}  

addTableToDatabase::String->IO()
addTableToDatabase tableName=do {
    currentDbHandle<-openFile currentDatabaseInfo ReadMode;
    currentDb<-hGetLine currentDbHandle;
    databaseHandle<-openFile currentDb AppendMode;
    hPutStrLn databaseHandle (tableName ++ ".table");
    hClose databaseHandle;
    hClose currentDbHandle;
}

getColumnInfosFromUser::Int->Int->IO [TColumnInfo]
getColumnInfosFromUser yCoord xCoord=do
{
  setCursorPosition yCoord xCoord;
  putStrLn "------------------------------------";
  setCursorPosition (yCoord+1) xCoord;
  putStr "Name of column:   ";
  hFlush stdout;
  nume<-getLine;
   
  setCursorPosition (yCoord+2) xCoord;
  putStr "Data type (string/int/float/double): ";
  hFlush stdout;
  dataType<-getLine;
  
  setCursorPosition (yCoord+3) xCoord;
  putStr "Length: ";
  hFlush stdout;
  maxLengthUserInput<-getLine;
  maxLength<-return (read maxLengthUserInput::Int);
 
  columnInfo<-return (ColumnInfo nume dataType maxLength);
  setCursorPosition (yCoord+4) xCoord;
  putStr "Add more columns(D,N):";
  hFlush stdout;
  ras<-getLine;
  
  uppras<-return (map toUpper ras);
  if (uppras =="D") 
  then do
  {
     lsti<-(getColumnInfosFromUser (yCoord + 6) xCoord);
     return (columnInfo:lsti)
  }
  else 
     (return [columnInfo]);          
}


showTable::TFile->String->Int->Int->IO()
showTable file tableName yCoord xCoord=do
{

  clearScreen;
  setCursorPosition 0 2;
  putStrLn (("Table: " ++ tableName));
  (Header header)<-return (head (records file));

  lungimeZonaAntet<-return (getZoneLength header);

  setCursorPosition yCoord xCoord;
  
  putStrLn (replicateCharacter '-' lungimeZonaAntet);

  showHeader header (yCoord+1) xCoord;

  setCursorPosition (yCoord+2) xCoord;
  putStrLn (replicateCharacter '-' lungimeZonaAntet);
  record<-return (tail (records file));
  showRecords record header (yCoord+4) xCoord;

  Just (lc,cc)<-getCursorPosition;
  hFlush stdout;
  setCursorPosition lc xCoord;
  putStr (replicateCharacter '-' lungimeZonaAntet);

  hFlush stdout;
  t<-getChar;
  putChar t;
}

showHeader::[TColumnInfo]->Int->Int->IO()
showHeader [] _ _=return ()
showHeader columnInfos xCoord yCoord=do
{
  columnInfo<-return (head columnInfos); 

  setCursorPosition xCoord yCoord;
  putStrLn ((name columnInfo));

  showHeader (tail columnInfos) xCoord (yCoord+(maxLength columnInfo))
}


showRecords::[TRecord]->[TColumnInfo]->Int->Int->IO()
showRecords [] _ _ _=return () 
showRecords records columnInfos xCoord yCoord=do
{
  showRecord (head records) columnInfos xCoord yCoord;
  showRecords (tail records) columnInfos (xCoord+1) yCoord;
}

showRecord::TRecord->[TColumnInfo]->Int->Int->IO()
showRecord (Record []) _ _ _=return ()
showRecord (Record listaValoriColoana) columnInfos xCoord yCoord=do
{
  showColumn (head listaValoriColoana) columnInfos xCoord yCoord;
  columnInfo<-return (head columnInfos);
  showRecord (Record (tail listaValoriColoana)) (tail columnInfos) (xCoord) (yCoord +(maxLength columnInfo )+2) 
}

showColumn::TColumnValue->[TColumnInfo]->Int->Int->IO()
showColumn vcol (columnInfos:rl) xCoord yCoord=do
{
  setCursorPosition xCoord yCoord;
  case vcol of{
    (S inf)->putStrLn inf;
    (I inf)->putStrLn (show inf);
    (F inf)->putStrLn (show inf);
    (D inf)->putStrLn (show inf);
 };   
}
-- 6. Insert into table

insertIntoTable::IO()
insertIntoTable = do {
        currentDbHandler<-openFile currentDatabaseInfo ReadMode;
        eof<-hIsEOF currentDbHandler;
        hClose currentDbHandler;
        if(eof == True) then do {
        setCursorPosition 15 10;
          putStr "The database is not selected.";
          a<-getLine;
          putStr a;
        }else do {
           setCursorPosition 15 10;
           printExistingTables;
           setCursorPosition 15 10;
           putStr "Name of the table: ";
           tableName<-getLine;
           insertToTable tableName;
        }
} 

insertToTable::String->IO()
insertToTable tableName = do{
 currentDbHandler<-openFile currentDatabaseInfo ReadMode;
    currentDbName<-hGetLine currentDbHandler;
    tablesHandler<-openFile currentDbName ReadMode;
    registeredTables<-(getExistingTablesFromDb tablesHandler);
    hClose currentDbHandler;
    hClose tablesHandler;
    if(elem (tableName ++ ".table") registeredTables) then do {
       readTFile<-readTFile tableName;
       addRecordsToTable readTFile tableName 4 10;
    } else do {
       setCursorPosition 10 23;
       putStr ("Table  " ++ tableName ++ " is not registered in the database.");
       inp<-getLine;
       putStr inp;
    }
}

addRecordsToTable::TFile->String->Int->Int->IO()
addRecordsToTable tfile tableName yCoord xCoord = do {
    clearScreen;
    setCursorPosition yCoord xCoord;
    newRecord<-getColumnValuesFromUser (columnInfos (head (records tfile))) [] yCoord xCoord;
    newFile<-return (File {records=(appendRecordListToRecords ((head (records tfile)):[]) (appendToRecords (tail (records tfile)) newRecord))});
    writeTFile newFile (tableName);
    setCursorPosition (yCoord+2) xCoord;
    putStr "Add one more row?(D,N): ";
    hFlush stdout;
    ras<-getLine;
    uppras<-return (map toUpper ras);
    if (uppras == "D") 
    then do
    {
        addRecordsToTable newFile tableName yCoord xCoord;
    }
    else return()
}

getColumnValuesFromUser::[TColumnInfo]->[TColumnValue]->Int->Int->IO TRecord
getColumnValuesFromUser columnInfos columnValues yCoord xCoord = do {
    if((length columnInfos) == 0)
    then do {
       return (Record columnValues);
    }
    else do {  
       columnValueResult <-getColumnValueFromUser (head columnInfos) yCoord xCoord 0;
        getColumnValuesFromUser (tail columnInfos) (appendTo columnValues (result columnValueResult)) (yCoord + 1 + (attempts columnValueResult)) xCoord;
    }
}


getColumnValueFromUser::TColumnInfo->Int->Int->Int->IO TColumnValueResult 
getColumnValueFromUser columnInfo yCoord xCoord wrongAttempts = do {
   setCursorPosition yCoord xCoord;
   putStr ((name columnInfo) ++ ": ");
   insertedValue<-getLine;
   case (dataType columnInfo) of
    {
    "string"->do
    {
          return (ColumnValueResult{result = (S insertedValue), attempts = wrongAttempts});
    };
    "int"->do
    {
       if((readMaybe insertedValue::Maybe Int) == Nothing)
       then do {
           setCursorPosition (yCoord + 1) xCoord;
           putStr "The value entered does not match the date type of the column (int).";
           getColumnValueFromUser columnInfo (yCoord + 2) xCoord (wrongAttempts + 2);
         } else do{

           return (ColumnValueResult{result = (I (read insertedValue::Int)), attempts = wrongAttempts});
        }
    };
      "double"->do
    {
       if((readMaybe insertedValue::Maybe Double) == Nothing)
        then do {
         setCursorPosition (yCoord + 1) xCoord;
         putStr "The value entered does not match the date type of the column (double).";
         getColumnValueFromUser columnInfo (yCoord + 2) xCoord (wrongAttempts + 2);
        } else do{
           return (ColumnValueResult{result = (D (read insertedValue::Double)), attempts = wrongAttempts});
    }
    };
    "float"->do
    {
          if((readMaybe insertedValue::Maybe Float) == Nothing)
      then do {
           setCursorPosition (yCoord + 1) xCoord;
           putStr "The value entered does not match the date type of the column (float).";
           getColumnValueFromUser columnInfo (yCoord + 2) xCoord (wrongAttempts + 2);
       } else do{
           return (ColumnValueResult{result = (F (read insertedValue::Float)), attempts = wrongAttempts});
        }
    };
   };
}

-- 7. View table

showTableToUI::IO()
showTableToUI = do {
        currentDbHandler<-openFile currentDatabaseInfo ReadMode;
        eof<-hIsEOF currentDbHandler;
        hClose currentDbHandler;
        if(eof == True) then do {
        setCursorPosition 15 10;
            putStr "The database is not selected.";
            a<-getLine;
            putStr a;
        } else do {
        setCursorPosition 20 10;
           putStr "Name of the table: ";
           tableName<-getLine;
           printTable tableName;
        }
}

printTable::String->IO()
printTable tableName = do {
    currentDbHandler<-openFile currentDatabaseInfo ReadMode;
    currentDbName<-hGetLine currentDbHandler;
    tablesHandler<-openFile currentDbName ReadMode;
    registeredTables<-(getExistingTablesFromDb tablesHandler);
    hClose currentDbHandler;
    hClose tablesHandler; 
    if(elem (tableName ++ ".table") registeredTables) then do {
    tfile<-readTFile tableName;
    showTable tfile tableName 1 1;
    setCursorPosition 17 10;
    } else do {
         setCursorPosition 16 15;
         putStr ("Table  " ++ tableName ++ " is not registered in the database.");
    }
}

-- 8. Exit

showTableFromTableName::String->IO()
showTableFromTableName tableName = do{
  tfileFromReadFromFile<-readTFile (tableName ++ ".table");
  showTable tfileFromReadFromFile tableName 1 1;
}

main::IO()
main=do
{
    clearScreen;
    drawUI; 
    selectedOption<-drawMenu;
    executeOption selectedOption;
}